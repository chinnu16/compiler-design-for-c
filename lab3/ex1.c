#include<stdio.h>
#include<stdlib.h>
#include<string.h>
char neha;
typedef struct token
{
	char lexemename[10];
	unsigned int row,col;
	char type[10];
	char scope;
	int argc;
	int size;
	char argv[30];
	char rtype[10];
	int id;
}token;

typedef struct ListElement
{
	token node;
	struct ListElement* next;
}ListElement;

ListElement* table[26];

void initialize()
{
	for(int i=0;i<26;i++)
		table[i]=NULL;
}

void display()
{
	for(int i=0;i<26;i++)
	{
		if(table[i]!=NULL)
		{
			ListElement* cur=table[i];
			while(cur!=NULL)
			{
				printf("%d\t%s\t%s\t%d\t%c\t%d\t%s\t%s\t\n",cur->node.id,cur->node.lexemename,cur->node.type,cur->node.size,cur->node.scope,cur->node.argc,cur->node.argv,cur->node.rtype);
				cur=cur->next;
			}
		}
	}
}

int HASH(char* str)
{
	int hvalue=0;
	int len=strlen(str);
	for(int i=0;i<len;i++)
	{
		hvalue+=(int)str[i];
	}
	hvalue%=26;
	return hvalue;
}

int search(char* str)
{
	int h=HASH(str);
	if(table[h]==NULL)
		return 0;
	else
	{
		ListElement* cur=table[h];
		while(cur!=NULL)
		{
			if(strcmp(cur->node.lexemename,str)==0)
			{
				//printf("node id %d",cur->node.id);
				return cur->node.id;

			}
			cur=cur->next;
		}
		return 0;
	}
}

int insert(token tk)
{
	int lexid=search(tk.lexemename);
	if(lexid!=0)
		return lexid;
	int val=HASH(tk.lexemename);
	ListElement* cur=(ListElement*)malloc(sizeof(ListElement));
	cur->node=tk;
	cur->next=NULL;
	if(table[val]==NULL){
		table[val]=cur;
	}
	else
	{
		ListElement* ele=table[val];
		while(ele->next!=NULL)
		{
			ele=ele->next;
		}
		ele->next=cur;
	}
	return 0;
}

void uparg(token tk,int arc,char k[])
{
	int f=HASH(tk.lexemename);
	ListElement* cur=table[f];
	while(cur!=NULL)
	{
		if(strcmp(tk.lexemename,cur->node.lexemename)==0)
		{
			strcpy(cur->node.argv,k);
			cur->node.argc=arc;
		}
		cur=cur->next;
	}
}

void toupr(char *s)
{
	int i=0;
	 while(s[i]!='\0') 
     {
        if((s[i]>='a' && s[i]<='z') || (s[i]>='A' && s[i]<='Z'))
        {
           s[i] -= 32;
        }
        i++;
     }
}

void getNextToken()
{
	int sam=1;
	int lg=0;
	token tk[50];
	int t=0;
	FILE *fp,*fout;
	char ca[2];
	int cb;
	ca[1]='\0';
	char str[50],tbuf[50];
    int line=1,col=0;
    int k,s=0;
    int sz=0;
    int ftokenid;
	fp=fopen("sam.c","r");
	if(fp==NULL)
	{
		printf("cannot open the file \n");
		exit(0);
	}
	ca[0]=getc(fp);
	while (ca[0] != EOF)
	{
		if(ca[0]=='/')
		{
			cb=getc(fp);
			if(cb=='/')
			{
				while(ca[0]!='\n')
					ca[0]=getc(fp);
			}
			else if(cb=='*')
			{
				ca[0]=getc(fp);
				cb=getc(fp);
				while(ca[0]!='*' && cb!='/')
				{
					ca[0]=cb;
					cb=getc(fp);
				}
			}
			else
				fseek(fp,-1,SEEK_CUR);
			continue;
			
		}
		if(ca[0]=='#' && s==0)
		{
			ca[0]=getc(fp);
			while(ca[0]!='\n')
				ca[0]=getc(fp);
			continue;
		}
		if((ca[0]>=97 && ca[0]<=122))	
		{
			strcat(str,ca);
			ca[0]=getc(fp);
			while( (ca[0]>=97 && ca[0]<=122) || (ca[0]>=48 && ca[0]<=57) || ca[0]=='_' )
			{
				strcat(str,ca);
				ca[0]=getc(fp);
			}
		//	printf("%s\n",str);
		}
					if(!strcmp(str, "auto") || !strcmp(str, "default")  
				    || !strcmp(str, "signed") || !strcmp(str, "enum")  
				    ||!strcmp(str, "extern") || !strcmp(str, "for")  
				    || !strcmp(str, "register") || !strcmp(str, "if")  
				    || !strcmp(str, "else")  || !strcmp(str, "int") 
				    || !strcmp(str, "while") || !strcmp(str, "do") 
				    || !strcmp(str, "break") || !strcmp(str, "continue")  
				    || !strcmp(str, "double") || !strcmp(str, "float") 
				    || !strcmp(str, "return") || !strcmp(str, "char") 
				    || !strcmp(str, "case") || !strcmp(str, "const") 
				    || !strcmp(str, "sizeof") || !strcmp(str, "long") 
				    || !strcmp(str, "short") || !strcmp(str, "typedef") 
				    || !strcmp(str, "switch") || !strcmp(str, "unsigned") 
				    || !strcmp(str, "void") || !strcmp(str, "static") 
				    || !strcmp(str, "struct") || !strcmp(str, "goto") 
				    || !strcmp(str, "union") || !strcmp(str, "volatile"))
						{
							//printf(" line number :%d col number :%d\n",line,col);
							printf("<%s>",str);
							strcpy(tk[t].lexemename,str);
							tk[t].row=line;
							tk[t].col=col;
							strcpy(tk[t].type,"keyword");
							strcpy(tk[t].argv,"");
							strcpy(tk[t].rtype,"");
							t++;
							col+=strlen(str);
							strcpy(tbuf,str);
							char a[10]="int";
							char b[10]="char";
							char c[10]="float";
							char d[10]="double";

							if(!strcmp(tbuf,a) || !strcmp(tbuf,c) || !strcmp(tbuf,b))
								sz=4;
							else if(!strcmp(tbuf,d))
								sz=8;
						}
						else
						{
							if(strlen(str)>0)
							{
									strcpy(tk[t].lexemename,str);
									tk[t].row=line;
									tk[t].col=col;
									
									t++;
									//printf("<--%s>",str);
									//printf("\n\n%c\n\n",ca[0]);
									if(ca[0]!='(' && ca[0]!=' ')
									{
										
										strcpy(tk[t-1].type,tbuf);
										tk[t-1].size=sz;
										if(lg==0)
											tk[t-1].scope='G';
										else
											tk[t-1].scope='L';
										
										//printf("\n\n%d\n\n",sam);
										strcpy(tk[t-1].argv,"");
										strcpy(tk[t-1].rtype,"");
										tk[t-1].argc=0;
										//printf("created token\n");
										int lexret=search(tk[t-1].lexemename);
										if(lexret==0){
											tk[t-1].id=sam++;
											printf("<id,%d>",sam-1);
										}
										else{
											tk[t-1].id=lexret;
											printf("<id,%d>",lexret);
										}
										insert(tk[t-1]);
										//printf("inserted into table\n");
									}
									else if( ca[0]=='(')
									{
										tk[t-1].id=sam++;
										//printf("\n\n%d\n\n",sam);
										strcpy(tk[t-1].type,"FUNC");
										strcpy(tk[t-1].rtype,tbuf);
										tk[t-1].size;
										if(lg==0)
											tk[t-1].scope='G';
										else
											tk[t-1].scope='L';
										ftokenid=t-1;
										int lexret=insert(tk[t-1]);
										if(lexret==0)
											printf("<id,%d>",sam-1);
										else
											printf("<id,%d>",lexret);

										//token for (

										strcpy(tk[t].lexemename,ca);
										tk[t].row=line;
										tk[t].col=col;
										strcpy(tk[t].type,"LB");
										t++;
										printf("<%c>",ca[0]);

										// code for counting the arguements
										char cas[2];
										char str2[10];
										cas[1]='\0';
										cas[0]=getc(fp);
										//printf("\n%c\n",cas[0]);
										int argcc=0;
										char holdstr[20];
										holdstr[0]='\0';
										while(cas[0]!=')')
										{
													if(cas[0]==' ')
													{
														while(cas[0]!=' ')
															cas[0]=getc(fp);
													}
													str2[0]='\0';
													if(cas[0]>=97 && cas[0]<=122)
													{
														strcat(str2,cas);
														cas[0]=getc(fp);
														while(cas[0]>=97 && cas[0]<=122)
														{
															strcat(str2,cas);
															cas[0]=getc(fp);
														}
														//printf("%s",str2);
													}
													if(!strcmp(str2, "double") || !strcmp(str2, "float") ||
												    !strcmp(str2, "char") ||!strcmp(str2,"int"))
														{
															argcc++;
															//printf(" line number :%d col number :%d\n",line,col);
															printf("<%s>",str2);
															strcpy(tk[t].lexemename,str2);
															tk[t].row=line;
															tk[t].col=col;
															strcpy(tk[t].type,str2);
															t++;
															col+=strlen(str2);
															strcpy(tbuf,str2);
															cas[0]=getc(fp);

															//continue;
															str2[0]='\0';
															//cas[0]=getc(fp);
															//printf("|%c",cas[0]);
															//cas[0]=getc(fp);
															//printf("|%c",cas[0]);
															if(cas[0]>=97 && cas[0]<=122)
															{
																strcat(str2,cas);
																cas[0]=getc(fp);
																while(cas[0]>=97 && cas[0]<=122)
																{
																	strcat(str2,cas);
																	cas[0]=getc(fp);
																}
																//printf("%s",str2);
															}
															if(strlen(str2)!=0)
															{
																char keep[10];
																strcpy(tk[t].lexemename,str2);
																tk[t].row=line;
																tk[t].col=col;
																
																t++;																
																strcpy(tk[t-1].type,tbuf);
																tk[t-1].size=sz;
																if(lg==0)
																	tk[t-1].scope='G';
																else
																	tk[t-1].scope='L';
																
																//printf("\n\n%d\n\n",sam);
																//printf("created token\n");
																
																int lexret=search(tk[t-1].lexemename);
																if(lexret==0){
																	tk[t-1].id=sam++;
																	printf("<id,%d>",sam-1);
																	sprintf(keep,"<id,%d>",sam-1);
																	strcat(holdstr,keep);
																}
																else{
																	tk[t-1].id=lexret;
																	sprintf(keep,"<id,%d>",lexret);
																	strcat(holdstr,keep);
																	printf("<id,%d>",lexret);																
																}
																insert(tk[t-1]);
																}
															}
														
														else if(cas[0]==',')
														{
															
															strcpy(tk[t].lexemename,ca);
															tk[t].row=line;
															tk[t].col=col;
															strcpy(tk[t].type,"SS");
															t++;
															printf("<%c>",cas[0]);															
															cas[0]=getc(fp);
															continue;

														}
														else if(cas[0]==' ')
														{
															cas[0]=getc(fp);
															col++;
															
															}
														else
														{
															strcpy(tk[t].type,tbuf);
															tk[t].size=sz;
															if(lg==0)
																tk[t].scope='G';
															else
																tk[t].scope='L';
															
															
															//printf("\n\n%d\n\n",sam);
															strcpy(tk[t].argv,"");
															strcpy(tk[t].rtype,"");
															tk[t].argc=0;
															//printf("created token\n");
															int lexret=search(tk[t].lexemename);
															if(lexret==0){
																tk[t].id=sam++;
																printf("<id,%d>",sam-1);
															}
															else{
																tk[t].id=lexret;
																printf("<id,%d>",lexret);
															}
															t++;
														}
														}
														// update number of arguments and parameters in the linked list;
														
														uparg(tk[ftokenid],argcc,holdstr);


														strcpy(tk[t].lexemename,ca);
														tk[t].row=line;
														tk[t].col=col;
														strcpy(tk[t].type,"Rb");
														t++;
														printf("<%c>",cas[0]);
										//code for counting the arguemtns



									ca[0]=getc(fp);

									}
							}
							col+=strlen(str);
						}

				  
			if(ca[0]=='\n'){
				line++;
				col=1;
				printf("\n");
			}
			else if(ca[0]=='\t'){
				col+=4;
				printf("<	>");
			}
			else if(ca[0]==' '){
				col+=1;
			}
			else
			{
				//col+=1;
				if(ca[0]>=48 && ca[0]<=57)
				{
					str[0]='\0';
					strcat(str,ca);
					ca[0]=getc(fp);
					while(ca[0]>=48 && ca[0]<=57)
					{
						strcat(str,ca);
						ca[0]=getc(fp);
					}
					fseek(fp,-1,SEEK_CUR);
					strcpy(tk[t].lexemename,str);
					tk[t].row=line;
					tk[t].col=col;
					strcpy(tk[t].type,"NUMBER");
					t++;
					printf("<%s>",str);
				}
				else if(ca[0]=='(')
				{
					strcpy(tk[t].lexemename,ca);
					tk[t].row=line;
					tk[t].col=col;
					strcpy(tk[t].type,"LB");
					t++;
					printf("<%c>",ca[0]);
				}
				else if (ca[0]==')')
				{
					strcpy(tk[t].lexemename,ca);
					tk[t].row=line;
					tk[t].col=col;
					strcpy(tk[t].type,"Rb");
					t++;
					printf("<%c>",ca[0]);
				}
				else if(ca[0]=='{')
				{
					strcpy(tk[t].lexemename,ca);
					tk[t].row=line;
					tk[t].col=col;
					strcpy(tk[t].type,"LC");
					t++;
					lg--;
					printf("<%c>",ca[0]);

				}
				else if(ca[0] == '}')
				{
					strcpy(tk[t].lexemename,ca);
					tk[t].row=line;
					tk[t].col=col;
					strcpy(tk[t].type,"Rc");
					t++;
					lg++;
					printf("<%c>",ca[0]);
				}
				else if(ca[0]=='+' || ca[0]=='-' || ca[0] =='*' || ca[0]=='/')
				{
					strcpy(tk[t].lexemename,ca);
					tk[t].row=line;
					tk[t].col=col;
					strcpy(tk[t].type,"AR OP");
					t++;
					printf("<%c>",ca[0]);
				}
				else if(ca[0]=='=')
				{
					int c;
					c=getc(fp);
					if (c=='=')
					{
						str[0]='\0';
						strcat(str,ca);
						ca[0]=c;
						strcat(str,ca);
						strcpy(tk[t].lexemename,str);
						tk[t].row=line;
						tk[t].col=col;
						strcpy(tk[t].type,"RLOP");
						t++;
						printf("<%s>",str);
					}
					else
					{
						strcpy(tk[t].lexemename,ca);
						tk[t].row=line;
						tk[t].col=col;
						strcpy(tk[t].type,"A OP");
						t++;
						printf("<%c>",ca[0]);
						fseek(fp,-1,SEEK_CUR);

					}
				}
				else if(ca[0]=='>' || ca[0]=='<')
				{
					str[0]='\0';
					strcat(str,ca);
					int c;
					c=getc(fp);
					if(c=='=')
					{
						ca[0]=c;
						strcat(str,ca);
					}
					else
					{
						fseek(fp,-1,SEEK_CUR);
					}
					strcpy(tk[t].lexemename,str);
					tk[t].row=line;
					tk[t].col=col;
					strcpy(tk[t].type,"relop");
					t++;
					printf("<%s>",str);

				}
				else if(ca[0]=='&')
				{
					int c=getc(fp);
					if(c=='&')
					{
						str[0]='\0';
						strcat(str,ca);
						ca[0]=c;
						strcat(str,ca);
						strcpy(tk[t].lexemename,str);
						tk[t].row=line;
						tk[t].col=col;
						strcpy(tk[t].type,"LOG");
						t++;
						printf("<%s>",str);
					}
					else
					{
						fseek(fp,-1,SEEK_CUR);
						strcpy(tk[t].lexemename,ca);
						tk[t].row=line;
						tk[t].col=col;
						strcpy(tk[t].type,"BIT op");
						t++;
						printf("<%c>",ca[0]);
					}
				}
				else if(ca[0]=='|')
				{
					int c=getc(fp);
					if(c=='|')
					{
						str[0]='\0';
						strcat(str,ca);
						ca[0]=c;
						strcat(str,ca);
						strcpy(tk[t].lexemename,str);
						tk[t].row=line;
						tk[t].col=col;
						strcpy(tk[t].type,"LOGICAL");
						t++;
						printf("<%s>",str);
					}
					else
					{
						fseek(fp,-1,SEEK_CUR);
						strcpy(tk[t].lexemename,ca);
						tk[t].row=line;
						tk[t].col=col;
						strcpy(tk[t].type,"BIT OP");
						t++;
						printf("<%c>",ca[0]);
					}
				}
				else if(ca[0]=='!')
				{
					strcpy(tk[t].lexemename,ca);
					tk[t].row=line;
					tk[t].col=col;
					strcpy(tk[t].type,"LOG OP");
					t++;
					printf("<%c>",ca[0]);
				}
				
				else if (ca[0] == '\"')
				{
					if(s>0)
						s--;
					else
						s++;
					str[0]='\0';
					ca[0]=getc(fp);
					while(ca[0]!='\"')
					{
						strcat(str,ca);
						ca[0]=getc(fp);
					}
					strcpy(tk[t].lexemename,str);
					tk[t].row=line;
					tk[t].col=col;
					strcpy(tk[t].type,"SR LIT");
					t++;
					printf("<%s>",str);

				}
				else
				{
					strcpy(tk[t].lexemename,ca);
					tk[t].row=line;
					tk[t].col=col;
					strcpy(tk[t].type,"SS");
					t++;
					printf("<%s>",ca);

				}
				col+=1;
			
			}				  
			str[0]='\0';
			ca[0]=getc(fp);
		}


	fclose(fp);
	printf("\n\n\n");
	fout=fopen("tokens","w");
	for(int i=0;i<t;i++)
	{
		fprintf(fout,"<%s,%d,%d,%s>",tk[i].lexemename,tk[i].row,tk[i].col,tk[i].type);

	}
}

int main()
{
	getNextToken();
	display();
}
	
