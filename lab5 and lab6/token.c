#include<stdio.h>
#include<string.h>
#include<stdlib.h>
/*
1.identifier
2.Keyword
3.number
4.41-left para
5.51-left bracket
6.semicolon
7.com operator;
8.operator
9.assignment
10,string
12.,
13..
141 142-[]
*/
typedef struct token
{
	char lexemename[50];
	int row,col;
	int type;
}token;

int l=1,c=1;
token* getNextToken(FILE *f)
{
	char ca[2],cb[2],str[10];
	ca[1]='\0';
	cb[1]='\0';
	c++;
	ca[0]=getc(f);
	if(ca[0]==EOF)
	{
			token *t=(token*)malloc(sizeof(token));
			strcpy(t->lexemename,"NULL");
			t->row=l;
			t->col=c;
			t->type=1;
			printf("end of file\n");
			return t;
	}
	if((ca[0]==' ' || ca[0]=='\n' || ca[0]=='\t') && ca[0]!=EOF)
	{
		if(ca[0]=='\n')
		{
			l++;
			c=1;
		}
		else if(ca[0]=='\t')
		{
			c+=4;
		}
		else
		{
			c++;
		}
		ca[0]=getc(f);
		while((ca[0]==' ' || ca[0]=='\n' ||ca[0]=='\t') && ca[0]!=EOF)
		{
			if(ca[0]=='\n')
			{
				l++;
				c=1;
			}
			else if(ca[0]=='\t')
			{
				c+=4;
			}
			else
			{
				c++;
			}
			c++;
			ca[0]=getc(f);
		}
	}
	if(ca[0]==EOF)
	{
		token *t=(token*)malloc(sizeof(token));
		strcpy(t->lexemename,"NULL");
		t->row=l;
		t->col=c;
		t->type=1;
		fseek(f,-1,SEEK_CUR);
		printf("end of file\n");
		return t;

	}
	int mcomm=1;
	while(mcomm==1)
	{
		if(ca[0]=='/')
			{
				c++;
				cb[0]=getc(f);
				if(cb[0]=='/')
				{
					c++;
					ca[0]=getc(f);
					while(ca[0]!='\n')
					{
						ca[0]=getc(f);
						c++;
					}
					l++;
					c=1;
					ca[0]=getc(f);
				}
				else if(cb[0]=='*')
				{
					c++;
					ca[0]=getc(f);
					cb[0]=getc(f);
					while(ca[0]!='*' && cb[0]!='/')
					{
						c++;
						ca[0]=cb[0];
						cb[0]=getc(f);
						if(ca[0]=='\n')
						{
							l++;c=1;
						}
					}
					c++;
					ca[0]=getc(f);
				}
				else		
				{
					fseek(f,-1,SEEK_CUR);
				}
			}
			if((ca[0]==' ' || ca[0]=='\n' || ca[0]=='\t') && ca[0]!=EOF)
			{
				if(ca[0]=='\n')
				{
					l++;
					c=1;
				}
				else if(ca[0]=='\t')
				{
					c+=4;
				}
				else
				{
					c++;
				}
				c++;
				ca[0]=getc(f);
				while((ca[0]==' ' || ca[0]=='\n' ||ca[0]=='\t') && ca[0]!=EOF)
				{
					if(ca[0]=='\n')
					{
						l++;
						c=1;
					}
					else if(ca[0]=='\t')
					{
						c+=4;
					}
					else
					{
						c++;
					}
					c++;
					ca[0]=getc(f);
				}
			}
			if(ca[0]=='/')
			{
				c++;
				cb[0]=getc(f);
				if(cb[0]=='/' || cb[0]=='*')
				{
					fseek(f,-1,SEEK_CUR);
				}
				else
				{
					fseek(f,-1,SEEK_CUR);
					mcomm=0;
				}
			}
			else
			{
				mcomm=0;
			}
	}
	if((ca[0]==' ' || ca[0]=='\n' || ca[0]=='\t') && ca[0]!=EOF)
	{
		if(ca[0]=='\n')
		{
			l++;
			c=1;
		}
		else if(ca[0]=='\t')
		{
			c+=4;
		}
		else
		{
			c++;
		}
		c++;
		ca[0]=getc(f);
		while((ca[0]==' ' || ca[0]=='\n' ||ca[0]=='\t') && ca[0]!=EOF)
		{
			if(ca[0]=='\n')
			{
				l++;
				c=1;
			}
			else if(ca[0]=='\t')
			{
				c+=4;
			}
			else
			{
				c++;
			}
			c++;
			ca[0]=getc(f);
		}
	}
	if(ca[0]==EOF)
	{
		token *t=(token*)malloc(sizeof(token));
		strcpy(t->lexemename,"NULL");
		t->row=l;
		t->col=c;
		t->type=1;
		fseek(f,-1,SEEK_CUR);
		printf("end of file\n");
		return t;

	}
	if(ca[0]=='#')
	{
		ca[0]=getc(f);c++;
		while(ca[0]!='\n')
		{
			ca[0]=getc(f);c++;
		}
		l++;c=0;
		ca[0]=getc(f);
	}
	if((ca[0]>='a' &&ca[0] <='z') || (ca[0]>='A' &&ca[0]<='Z'))
	{
		str[0]='\0';
		strcat(str,ca);
		ca[0]=getc(f);c++;
		while((ca[0]>='a' && ca[0]<='z')||(ca[0]>='A' && ca[0]<='Z')||(ca[0]>=48 && ca[0]<=57) || ca[0]=='_')
		{
			strcat(str,ca);
			ca[0]=getc(f);c++;
		}
		if(!strcmp(str, "auto") || !strcmp(str, "default")  
				    || !strcmp(str, "signed") || !strcmp(str, "enum")  
				    ||!strcmp(str, "extern") || !strcmp(str, "for")  
				    || !strcmp(str, "register") || !strcmp(str, "if")  
				    || !strcmp(str, "else")  || !strcmp(str, "int") 
				    || !strcmp(str, "while") || !strcmp(str, "do") 
				    || !strcmp(str, "break") || !strcmp(str, "continue")  
				    || !strcmp(str, "double") || !strcmp(str, "float") 
				    || !strcmp(str, "return") || !strcmp(str, "char") 
				    || !strcmp(str, "case") || !strcmp(str, "const") 
				    || !strcmp(str, "sizeof") || !strcmp(str, "long") 
				    || !strcmp(str, "short") || !strcmp(str, "typedef") 
				    || !strcmp(str, "switch") || !strcmp(str, "unsigned") 
				    || !strcmp(str, "void") || !strcmp(str, "static") 
				    || !strcmp(str, "struct") || !strcmp(str, "goto") 
				    || !strcmp(str, "union") || !strcmp(str, "volatile") || !strcmp(str,"main"))
		{
			token *t=(token*)malloc(sizeof(token));
			strcpy(t->lexemename,str);
			t->row=l;
			t->col=c;
			t->type=2;
			fseek(f,-1,SEEK_CUR);
			return t;
		}
		else if(strlen(str)>0)
		{
			token *t=(token*)malloc(sizeof(token));
			strcpy(t->lexemename,"id");
			t->row=l;
			t->col=c;
			t->type=1;
			fseek(f,-1,SEEK_CUR);
			return t;
		}
	}
	if(ca[0]>=48 && ca[0]<=57)
	{
		str[0]='\0';
		strcpy(str,ca);
		ca[0]=getc(f);
		while(ca[0]>=48 && ca[0]<=58)
		{
			strcpy(str,ca);
			ca[0]=getc(f);c++;
		}
		token *t=(token*)malloc(sizeof(token));
		strcpy(t->lexemename,"num");
		t->row=l;
		t->col=c;
		t->type=3;
		fseek(f,-1,SEEK_CUR);
		return t;
	}
	if(ca[0]==')')
	{
		str[0]='\0';
		strcat(str,ca);
		token *t=(token*)malloc(sizeof(token));
		strcpy(t->lexemename,str);
		t->row=l;
		t->col=c;
		t->type=41;
		return t;

	}
	if(ca[0]==',')
	{
		str[0]='\0';
		strcat(str,ca);
		token *t=(token*)malloc(sizeof(token));
		strcpy(t->lexemename,str);
		t->row=l;
		t->col=c;
		t->type=12;
		return t;

	}
		if(ca[0]=='.')
	{
		str[0]='\0';
		strcat(str,ca);
		token *t=(token*)malloc(sizeof(token));
		strcpy(t->lexemename,str);
		t->row=l;
		t->col=c;
		t->type=13;
		return t;

	}
	if(ca[0]=='[')
	{
		str[0]='\0';
		strcat(str,ca);
		token *t=(token*)malloc(sizeof(token));
		strcpy(t->lexemename,str);
		t->row=l;
		t->col=c;
		t->type=141;
		return t;

	}
	if(ca[0]==']')
	{
		str[0]='\0';
		strcat(str,ca);
		token *t=(token*)malloc(sizeof(token));
		strcpy(t->lexemename,str);
		t->row=l;
		t->col=c;
		t->type=142;
		return t;

	}
	else if(ca[0]=='(')
	{
		str[0]='\0';
		strcat(str,ca);
		token *t=(token*)malloc(sizeof(token));
		strcpy(t->lexemename,str);
		t->row=l;
		t->col=c;
		t->type=42;
		return t;

	}
	else if(ca[0]=='{')
	{
		str[0]='\0';
		strcat(str,ca);
		token *t=(token*)malloc(sizeof(token));
		strcpy(t->lexemename,str);
		t->row=l;
		t->col=c;
		t->type=51;
		return t;

	}
	else if(ca[0]=='$')
	{
		str[0]='\0';
		strcat(str,ca);
		token *t=(token*)malloc(sizeof(token));
		strcpy(t->lexemename,str);
		t->row=l;
		t->col=c;
		t->type=51;
		return t;

	}
	else if(ca[0]=='}')
	{
		str[0]='\0';
		strcat(str,ca);
		token *t=(token*)malloc(sizeof(token));
		strcpy(t->lexemename,str);
		t->row=l;
		t->col=c;
		t->type=52;
		return t;


	}
	else if(ca[0]=='+' || ca[0] == '-' || ca[0]=='*' || ca[0]=='/')
	{
		str[0]='\0';
		strcat(str,ca);
		cb[0]=getc(f);c++;
		if(cb[0]=='=')
		{
			strcat(str,cb);				
			token *t=(token*)malloc(sizeof(token));
			strcpy(t->lexemename,str);
			t->row=l;
			t->col=c;
			t->type=7;
			return t;
		}
		else
		{
			token *t=(token*)malloc(sizeof(token));
			strcpy(t->lexemename,str);
			t->row=l;
			t->col=c;
			t->type=8;
			fseek(f,-1,SEEK_CUR);c--;
			return t;
		}

	}
	else if(ca[0]=='=')
	{
		str[0]='\0';
		strcat(str,ca);
		cb[0]=getc(f);c++;
		if(cb[0]=='=')
		{
			strcat(str,cb);
			token *t=(token*)malloc(sizeof(token));
			strcpy(t->lexemename,str);
			t->row=l;
			t->col=c;
			t->type=9;
			return t;
		}
		else
		{
			token *t=(token*)malloc(sizeof(token));
			strcpy(t->lexemename,str);
			t->row=l;
			t->col=c;
			t->type=8;
			fseek(f,-1,SEEK_CUR);c--;
			return t;
		}

	}
	else if(ca[0]==';')
	{
		str[0]='\0';
		strcat(str,ca);
		token *t=(token*)malloc(sizeof(token));
		strcpy(t->lexemename,str);
		t->row=l;
		t->col=c;
		t->type=6;
		return t;
	}
	else if(ca[0]=='"')
	{
		ca[0]=getc(f);c++;
		str[0]='\0';
		while(ca[0]!='"')
		{
			strcat(str,ca);
			ca[0]=getc(f);
		}
			token *t=(token*)malloc(sizeof(token));
			strcpy(t->lexemename,str);
			t->row=l;
			t->col=c;
			t->type=10;
			return t;	

	}
	else if(ca[0]=='>' ||ca[0]=='<')
	{
		str[0]='\0';
		strcat(str,ca);
		cb[0]=getc(f);c++;
		if(cb[0]=='=')
		{
			strcat(str,cb);
			token *t=(token*)malloc(sizeof(token));
			strcpy(t->lexemename,str);
			t->row=l;
			t->col=c;
			t->type=8;
			return t;
		
		}
		else
		{
			token *t=(token*)malloc(sizeof(token));
			strcpy(t->lexemename,str);
			t->row=l;
			t->col=c;
			t->type=8;
			fseek(f,-1,SEEK_CUR);c--;
			return t;
		}	
	}
}

/*int main()
{
	FILE *f=fopen("sam.c","r");
	if(f==NULL){
		printf("open problem");
	}
	token *t=getNextToken(f);
	printf("%s\n",t->lexemename);
	t=getNextToken(f);
	printf("%s\n",t->lexemename);
	t=getNextToken(f);
	printf("%s\n",t->lexemename);
	t=getNextToken(f);
	printf("%s\n",t->lexemename);
	//char ch=getc(f);
	//printf("%c--s",ch);
	t=getNextToken(f);
	printf("%s\n",t->lexemename);
	
}
*/