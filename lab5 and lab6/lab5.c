#include<stdio.h>
#include<string.h>
#include "token.c"
int td=0,mflag=0;
token *lk;

void program();
void dec();
void dt();
void id_list();
void stt_list();
void stt();
void assign_stat();
void expn();
void eprime();
void simple_expr();
void seprime();
void term();
void tprime();
void factor();
void relop();
void addop();
void mulop();
void decision_stat();
void dprime();
void looping_stat();
void invalid();

void invd(token *t,char* ch)
{
		printf("error in row --%d column--%d\n excepted %s\n",t->row,t->col,ch);
		exit(0);
}

void program(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;

	if(strcmp(lk->lexemename,"main")==0)
	{
		lk=getNextToken(f);
		if(strcmp(lk->lexemename,"(")==0)
		{
			lk=getNextToken(f);
			if(strcmp(lk->lexemename,")")==0)
			{
				lk=getNextToken(f);
				if(strcmp(lk->lexemename,"{")==0)
				{
					dec(f);
					stt_list(f);
					if(td==0)
						lk=getNextToken(f);
					else 
						td=0;
					//printf("row numer --%d",lk->row);
					if(strcmp(lk->lexemename,"}")==0)
						return;
				}
				else
				{
					invd(lk,"}");
				}
			}
			else
			{
				invd(lk,")");
			}
		}
		else
			{
				invd(lk,"(");	
			}
	}
	else
	{
		printf("excpeted main\n");
		char c='.';
		invd(lk,"main");
	}
}
void dec(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;

	if(strcmp(lk->lexemename,"int")==0 ||
	 strcmp(lk->lexemename,"char")==0 || 
	 strcmp(lk->lexemename,"float")==0 || 
	 strcmp(lk->lexemename,"double")==0)
	{
		id_list(f);
		if(td==0)
			lk=getNextToken(f);
		else
			td=0;
		if(strcmp(lk->lexemename,";")==0){
			dec(f);
			return;
		}
		else
		{
			invd(lk,";");
			exit(0);
		}
	}
	else
	{
		td=1;
		return;
	}
}

void id_list(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;

	if(strcmp(lk->lexemename,"id")==0)
	{
		lk=getNextToken(f);
		if(strcmp(lk->lexemename,",")==0)
		{
			id_list(f);
		}
		else if(strcmp(lk->lexemename,"[")==0)
		{
			lk=getNextToken(f);
			if(strcmp(lk->lexemename,"num")==0)
			{
				lk=getNextToken(f);
				if(strcmp(lk->lexemename,"]")==0)
				{
					lk=getNextToken(f);
					if(strcmp(lk->lexemename,",")==0)
					{
						id_list(f);
					}
					else
					{
						td=1;
						return;
					}
				}
				else
				{
					invd(lk,"]");
				}
			}
			else
			{
				invd(lk,"[");
			}
			
		}
		else
		{
			td=1;
			return;
		}
	}
}

void stt_list(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;
	if(strcmp(lk->lexemename,"id")==0 ||
		strcmp(lk->lexemename,"for")==0 || 
		strcmp(lk->lexemename,"while")==0 ||
		strcmp(lk->lexemename,"if")==0 )
	{
		td=1;
		stt(f);
		stt_list(f);
		return;
	}
	else if(strcmp(lk->lexemename,"}")==0)
	{
		td=1;
		return;
	}
	else
	{
		printf("%s\nerror --- stt_list\nfailure",lk->lexemename);
		exit(0);
	}

}

void stt(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;
	if(strcmp(lk->lexemename,"id")==0)
	{
		td=1;
		assign_stat(f);
		if(td==0)
			lk=getNextToken(f);
		else
			td=0;
		if(strcmp(lk->lexemename,";")==0)
			return;
		else
		{
			printf("func stt \n assigned statemtn not ended with semicolin\n");
			invd(lk,";");
		}

	}
	else if(strcmp(lk->lexemename,"if")==0)
	{
		td=1;
		decision_stat(f);
	}
	else if(strcmp(lk->lexemename,"while")==0 || strcmp(lk->lexemename,"for")==0)
	{
		td=1;
		looping_stat(f);
		return;
	}

	else if(strcmp(lk->lexemename,"}")==0)
	{
		td=1;
		return;
	}
	else
	{
		invd(lk," if while for id");
		printf("error --- %s--STT\n",lk->lexemename);
	}

}

void decision_stat(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;
	if(strcmp(lk->lexemename,"if")==0)
	{
		lk=getNextToken(f);
		if(strcmp(lk->lexemename,"(")==0)
		{
			expn(f);
			if(td==0)
				lk=getNextToken(f);
			else
				td=0;
			if(strcmp(lk->lexemename,")")==0)
			{
				lk=getNextToken(f);
				if(strcmp(lk->lexemename,"{")==0)
				{
					stt_list(f);
				}
				if(td==0)
					lk=getNextToken(f);
				else
					td=0;
				if(strcmp(lk->lexemename,"}")==0)
				{
					dprime(f);
					return;
				}
			}
		}
	}
	printf("error -- decision_stat\nfailure");
	exit(0);
}

void dprime(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;

	if(strcmp(lk->lexemename,"else")==0)
	{
		lk=getNextToken(f);
		if(strcmp(lk->lexemename,"{")==0)
		{
			stt_list(f);
			if(td==0)
				lk=getNextToken(f);
			else
				td=0;
			if(strcmp(lk->lexemename,"}")==0)
			{
				return;
			}
		}
	}
	else if(strcmp(lk->lexemename,"id")==0 ||
		strcmp(lk->lexemename,"while")==0 ||
		strcmp(lk->lexemename,"for")==0 ||
		strcmp(lk->lexemename,"if")==0 ||
		strcmp(lk->lexemename,"}")==0)

	{
		td=1;
		return;
	}
	else
	{
		invd(lk,"id for if while");
	}
}

void looping_stat(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;

	if(strcmp(lk->lexemename,"while")==0)
	{
		lk=getNextToken(f);
		if(strcmp(lk->lexemename,"(")==0)
		{
			expn(f);
			if(td==0)
				lk=getNextToken(f);
			else
				td=0;
			if(strcmp(lk->lexemename,")")==0)
			{
				lk=getNextToken(f);
				if(strcmp(lk->lexemename,"{")==0)
				{
					stt_list(f);
					if(td==0)
						lk=getNextToken(f);
					else td=0;
					if(strcmp(lk->lexemename,"}")==0)
					{
						return;
					}
					else
					{
						printf("error in while loop\nfailure");
						invd(lk,"}");
					}
				}
				else
				{
					printf("error in while loop\nfailure");
					invd(lk,"{");
				}
			}
			else
			{
				printf("error in while loop\nfailure");
				invd(lk,")");
			}
		}
		else
		{
			printf("error in while loop\nfailure");
			invd(lk,"(");
		}
		exit(0);
	}
	else if(strcmp(lk->lexemename,"for")==0)
	{
		lk=getNextToken(f);
		if(strcmp(lk->lexemename,"(")==0)
		{
			assign_stat(f);
			if(td==0)
				lk=getNextToken(f);
			else 
				td=0;
			//printf("%s\nasignment completed\n",lk->lexemename);
			if(strcmp(lk->lexemename,";")==0)
			{
				expn(f);
				if(td==0)
					lk=getNextToken(f);
				else
					td=0;
			//	printf("%s\n",lk->lexemename );
				if(strcmp(lk->lexemename,";")==0)
				{
					assign_stat(f);
					if(td==0)
						lk=getNextToken(f);
					else 
						td=0;
			//		printf("%s\nasignment completed\n",lk->lexemename);
					if(strcmp(lk->lexemename,")")==0)
					{

						lk=getNextToken(f);
						if(strcmp(lk->lexemename,"{")==0)
						{
							//printf("finally entered\n");
							stt_list(f);
							if(td==0)
								lk=getNextToken(f);
							else
								td=0;
							if(strcmp(lk->lexemename,"}")==0)
							{
								return;
							}
							else
							{
								invd(lk,"}");
							}
						}
						else
						{
							invd(lk,"{");
						}
					}
					else
					{
						invd(lk,")");
					}
				}
				else
				{
					invd(lk,"2--;");
				}
			}
			else
			{
				invd(lk,"1-;");
			}
		}
		else
		{
			invd(lk,"(");
		}
		printf("error in for");
		exit(0);
	}
	else
	{
		printf("error---LOOPING STAT\nfailure");
		exit(0);
	}
}

void assign_stat(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;
	if(strcmp(lk->lexemename,"id")==0)
	{
		lk=getNextToken(f);
		if(strcmp(lk->lexemename,"=")==0)
		{
			expn(f);
			return;
		}
	}
	else if(strcmp(lk->lexemename,";")==0)
	{
		td=1;
		return;
	}
	else
	{
		printf("eror --- assign_stat\nfailure\n");
		invd(lk,";");
		exit(0);
	}
}

void expn(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;

	if(strcmp(lk->lexemename,"id")==0 || strcmp(lk->lexemename,"num")==0)
	{
		td=1;
		simple_expr(f);
		eprime(f);
		return;
	}	
	else
	{
		printf("error---EXPN\nfailure");
		invd(lk,"id num");
	}
}
 
void eprime(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;

	if(strcmp(lk->lexemename,"<=")==0 ||
		strcmp(lk->lexemename,">=")==0 ||
		strcmp(lk->lexemename,"<")==0 ||
		strcmp(lk->lexemename,">")==0 ||
		strcmp(lk->lexemename,"!=")==0 ||
		strcmp(lk->lexemename,"==")==0 
		)
	{
		td=1;
		relop(f);
		//printf("relop -- %s\n",lk->lexemename );
		simple_expr(f);
		return;
	}
	else if(strcmp(lk->lexemename,";")==0||
		strcmp(lk->lexemename,"id")==0 ||
		strcmp(lk->lexemename,")")==0)
	{
		//printf("eprime-)\n");
		td=1;
		return;
	}
	else
	{
		printf("%s---eprime\n",lk->lexemename);
		invd(lk,"; or id or )");
	}

} 
void simple_expr(FILE *f)
{
	if(mflag==1)
	return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;
	if(strcmp(lk->lexemename,"id")==0 || strcmp(lk->lexemename,"num")==0)
	{
		td=1;
		term(f);
		seprime(f);
		return;
	}
	else
	{
		printf("%s\n",lk->lexemename );
		lk=getNextToken(f);
		printf("%s\n",lk->lexemename );
		invd(lk,"id or num\nsimple_expr");
	}
}


void seprime(FILE *f)
{
	if(mflag==1)
	return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;
	if(strcmp(lk->lexemename,"+")==0||strcmp(lk->lexemename,"-")==0)	
	{
		td=1;
		addop(f);
		term(f);
		seprime(f);
	}
	else if(strcmp(lk->lexemename,";")==0 ||
		strcmp(lk->lexemename,")")==0)
	{
		td=1;
		return;
	}
	else if(strcmp(lk->lexemename,">=")==0 ||
		strcmp(lk->lexemename,"<=")==0 ||
		strcmp(lk->lexemename,"==")==0 ||
		strcmp(lk->lexemename,">")==0 ||
		strcmp(lk->lexemename,"<")==0||
		strcmp(lk->lexemename,"!=")==0)
	{
		td=1;
		return;
	}
	else
	{
		printf("%s\n",lk->lexemename );
		invd(lk,";\n seprime");		
	}
}

void term(FILE *f)
{
	if(mflag==1)
	return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;

	if(strcmp(lk->lexemename,"id")==0 ||strcmp(lk->lexemename,"num")==0)
	{
		td=1;
		factor(f);
		tprime(f);
		return;
	}
	else
	{
		printf("error---TERM\nfail'");
		invd(lk,";");
		exit(0);
	}

}

void tprime(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;

	if(strcmp(lk->lexemename,"*")==0 || strcmp(lk->lexemename,"/")==0 || strcmp(lk->lexemename,"%")==0)
	{
		td=1;
		mulop(f);
		factor(f);
		tprime(f);
		return;
	}
	else if(strcmp(lk->lexemename,"+")==0 || 
		strcmp(lk->lexemename,"-")||
		strcmp(lk->lexemename,";"))
	{
		td=1;
		return ;
	}
	else
	{
		invd(lk,"+ - ;\ntprime");
	}
}

void mulop(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;

	if(strcmp(lk->lexemename,"*")==0 || strcmp(lk->lexemename,"/")==0 || strcmp(lk->lexemename,"%")==0)
	{
		return;
	}
	else
	{
		printf("errro -- MULOP\nfailure");
		invd(lk,"* % /");
		exit(0);
	}
}

void addop(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;

	if(strcmp(lk->lexemename,"+")==0 || strcmp(lk->lexemename,"-")==0)
	{
		return;
	}
	else
	{

		printf("error -- ADDOP\n failure");
		invd(lk,"+ - ");
		exit(0);
	}
}

void relop(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;

	if(strcmp(lk->lexemename,"==")==0 ||
	 strcmp(lk->lexemename,">=")==0 ||
	  strcmp(lk->lexemename,"<=")==0 ||
	  strcmp(lk->lexemename,"<")==0 ||
	   strcmp(lk->lexemename,">")==0||
	   strcmp(lk->lexemename,"!=")==0 
	   )
	{
		return;
	}
	else
	{
		printf("error in relop---REOP\nfailure");
		invd(lk,"== <= >= != > <");
		exit(0);
	}
}

void factor(FILE *f)
{
	if(mflag==1)
		return;

	if(td==0)
		lk=getNextToken(f);
	else
		td=0;

	if(strcmp(lk->lexemename,"id")==0 || strcmp(lk->lexemename,"num")==0)
		return;
	else
	{
		printf("error---FACTOR\nfailure");
		invd(lk,"id num");
		exit(0);
	}

}



int main()

{
	FILE *f=fopen("test1.c","r");
	if(f==NULL)
	{
		printf("cannot open the file\n");
		return 0;
	}
	td=0;
	program(f);
	if(td==0)
		lk=getNextToken(f);
	if(strcmp(lk->lexemename,"$")==0)
		printf("compiled successfully\n");
	else
		printf("failure");
}