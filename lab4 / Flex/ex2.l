%{
int words=0;
int c=0;
int lines=0;
int blanks=0;	
%}

%%
[a-zA-Z]+ {
	words++;
	c+=strlen(yytext);
}
\n {
	lines++;
	c++;
}
. {
	blanks++;
	c++;
}
%%

int yywrap(){return 1;}

void main()
{
	yyin = fopen("sam.c","r");
	yylex();
	printf("words - %d\nchar - %d\nlines - %d\nblanks - %d\n",words,c,lines,blanks);
}
