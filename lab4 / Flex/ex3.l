%{
int pint=0;
int nint=0;
int pfl=0;
int nflo=0;
%}

%%
-[0-9]*\.[0-9]+ {
	nflo++;
}
-[0-9]+ {
	nint++;
}
\+*[0-9]*\.[0-9]* {
	pfl++;
}
\+*[0-9]+ {
	pint++;
}
%%

int yywrap(){return 1;}

void main()
{
	yyin = fopen("sam.c","r");
	yylex();
	printf("pint - %d\n nint - %d\npflo - %d\nnflo - %d\n",pint,nint,pfl,nflo);
}