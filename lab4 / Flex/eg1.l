%{
int chars=0;
int words=0;
int lines=0;
int blanks=0;
%}

%%
[ ]+ {
blanks++;
}
[a-zA-z]+ {
words++;
chars+=strlen(yytext);
}
. {
	lines++;
}
%%

int yywrap(){return 0;}

void main()
{
	yyin=fopen("sam.c","r");
	yylex();
	printf("%d  %d  %d %d\n",lines,words,chars,blanks);
}
